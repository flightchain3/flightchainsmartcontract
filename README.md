# Flight Chain Smart Contract v3
Extended version of "FlightChain" to support passenger related counts including:
 - Total Pax on board
 - total Checked Bags on board
 - transfer pax & bag counts
 - Pax requiring assistance counts
 
 

## Getting Started

### Pre-requisites
You must have node v9 installed and all the hyperledger fabric requirements installed - see top level project README

Build uses gulp to perform build tasks
```
npm install -g gulp-cli
```

Install npm modules
```
npm install
```

## Deploying chaincode locally


There is a demo local fabric network in [sita-basic-network](./sita-basic-network). This script will instantiate
that network and deploy the chaincode onto it. 

> **Note 1** if the script does not execute - you may need to set the execute permisssions for this and all other .sh files 'chmod +x *.sh'

> **Note 2** if you re-use an existing version number then the docker image will not be rebuilt and your newly changed code will not be deployed onto the network - you'll simply redeploy the old code. If redeploying onto a machine which previously had an old version, the instantiate chaincode may time out..the easiest fix is to increment the version number - e.g 1.1, 1.2 etc

> **Note 3** Install the Hyperledger Fabric platform-specific binaries and config files for the version specified into the /bin and /config directories of fabric-samples.
Download the Hyperledger Fabric docker images using the curl command 
`curl -sSL http://bit.ly/2ysbOFE | bash -s` .
> The command downloads and executes a bash script that will download and extract all of the platform-specific binaries you will need to set up your network and place them into the cloned repo you created above.
It retrieves the following platform-specific binaries and places them in the bin sub-directory of the current working directory.
```
configtxgen,
configtxlator,
cryptogen,
discover,
idemixgen
orderer,
peer, and
fabric-ca-client
```
> **Note 4** You may want to add that to your PATH environment variable so that these can be picked up without fully qualifying the path to each binary. e.g.:
```
export PATH=<path to download location>/bin:$PATH
```

Run `./deployChainCode.sh -v 1.0 -n flightchain` to rebuild the network and redeploy the chaincode.
After you run this command, the network should be running with the chain code deployed.

Run `docker ps` and the output should look like this:

```
KOS:flight-chain-ui kosullivan$ docker ps
CONTAINER ID        IMAGE                                                                                                          COMMAND                  CREATED              STATUS              PORTS                                            NAMES
d9b1ed5fefee        dev-peer1.sandbox.sita.aero-flightchain-3.0-21e75a3d558c03e0e970addf42d4e53ba22d869d1bc897d9b7d823c981d37fcd   "/bin/sh -c 'cd /usr…"   8 seconds ago        Up 7 seconds                                                         dev-peer1.sandbox.sita.aero-flightchain-3.0
c0b4009944e0        dev-peer0.sandbox.sita.aero-flightchain-3.0-b7b1b6748265eb50c35d538932b1eaa68f4e40425613204d4f3ecd54f681e900   "/bin/sh -c 'cd /usr…"   41 seconds ago       Up 40 seconds                                                        dev-peer0.sandbox.sita.aero-flightchain-3.0
d48a726cd14b        hyperledger/fabric-peer                                                                                        "peer node start"        About a minute ago   Up About a minute   0.0.0.0:8051->7051/tcp, 0.0.0.0:8053->7053/tcp   peer1.sandbox.sita.aero
023387d7ca0f        hyperledger/fabric-peer                                                                                        "peer node start"        About a minute ago   Up About a minute   0.0.0.0:7051->7051/tcp, 0.0.0.0:7053->7053/tcp   peer0.sandbox.sita.aero
8a47926217ac        hyperledger/fabric-couchdb                                                                                     "tini -- /docker-ent…"   About a minute ago   Up About a minute   4369/tcp, 9100/tcp, 0.0.0.0:5984->5984/tcp       couchdb
aff801facf97        hyperledger/fabric-tools                                                                                       "/bin/bash"              About a minute ago   Up About a minute                                                    cli
6ea5f904fcc8        hyperledger/fabric-ca                                                                                          "sh -c 'fabric-ca-se…"   About a minute ago   Up About a minute   0.0.0.0:7054->7054/tcp                           ca.sita.aero
38e17797c4d2        hyperledger/fabric-orderer                                                                                     "orderer"                About a minute ago   Up About a minute   0.0.0.0:7050->7050/tcp                           orderer.sita.aero
```

## Cleaning up a deployment

To tear down the docker containers:
```
cd sita-basic-network
./stop.sh
./teardown.sh
```


### building via gulp
developers should install ts-node globally to be able run these commands:
```
npm i -g ts-node
```

gulp is a js task automation utility used to perform common actions in a consistent manner:

1. Install the gulp cli
```
npm install -g gulp-cli
```

2. run typescript compile
```
gulp compile
```

3. run tests
```
gulp test
```

4. run all build steps
```
gulp
```

## Developer Notes

### structure
- dist (destination for output JS) - **needs to be deployed**
- META-INF (contains indexes for rich queries) - **needs to be deployed**
- node_modules (3rd part dependencies) - **needs to be deployed**
- privateData (private data collection def - **needs to be deployed**
- schema (JSON schema for ACRIS model)
- spec (unit tests)
- src (source code)
- package.json (npm package def) - **needs to be deployed**

### Updating the schema
Schema is defined in json format under the ./schema folder. This can be used to generate the ./src/model/ACRISFlight class definition used by the code

Do not update the code directly, instead update the appropriate schema json file(s) and run json2ts command to generate code

```
cd schema
..\node_modules\.bin\json2ts --input ACRISFlight.schema.json --output ..\src\model\ACRISFlight.ts
cd ..
```

### Manual Interacting via peer commands
#### deployment
*NOTE*: should not generally need to do this - only for those modifying the chaincode and wishing to redeploy
1. get the chaincode onto cli node
Note: need to have package.json, dist folder and node_modules folder

2. install on each node
```
CORE_PEER_ADDRESS=peer1st-sitaeu.fabric12:7051
peer chaincode install -n flightchain -v 1.0 -l node -p {TODO/path/to/the/chaincode}
# REPEAT for other peers - note Peer address and port may be different depending on how you have configured your network
```

3. instantiate on the channel
```
peer --logging-level debug chaincode instantiate -o orderer.sita.aero:7050 -C channel-flight-chain -n flightchain -v 1.0 -l node -c '{"Args":["FlightChainContract:initLedger"]}' -P "OR ('SITAMSP.member','Org2MSP.member')"
```
Note:  need to supply the init() method name in the Args (this seems to be different to previous fabric)

if you want to work with private data collections need to add: --collections-config arg
```
peer --logging-level debug chaincode upgrade -o orderer.sita.aero:7050 -C channel-flight-chain -n flightchain -v 1.4 -l node -c '{"Args":["FlightChainContract:initLedger"]}' -P "OR ('SITAMSP.member','Org2MSP.member')" --collections-config {TODO/path/to/the/chaincode}/privateData/collectionConfig.json
```

Note: if working with a TLS enabled network (which you should for production deploys) then you need to add  --tls --cafile=$ORDERER_CA to the instantiate/upgrade commands above

#### query and invocation examples
get the version (based on package json)
```
peer chaincode query -C channel-flight-chain -n flightchain -c '{"Args":["getVersion"]}'
```

create a flight (TODO correct but missing cert - think you need to use nodeSDK to do invokes)
```
peer chaincode invoke -o orderer.sita.aero:7050 -C channel-flight-chain -n flightchain -c '{"Args":["createFlight", "{\"operatingAirline\": {\"iataCode\": \"EI\"},\"departureAirport\": \"DUB\",\"arrivalAirport\": \"JFK\",\"flightNumber\": {\"trackNumber\":\"1234\"},\"originDate\": \"2019-03-17\",\"departure\": {\"scheduled\": \"2017-04-05T12:27:00-04:00\",\"estimated\":\"2017-04-05T12:27:00-04:00\",\"terminal\": \"N\",\"gate\": \"D47\"}}"]}'
```

get all flights for a date (simliar syntax can be used for other rich queries - see [couchdb docs](http://docs.couchdb.org/en/latest/api/database/find.html#find-selectors))
```
peer chaincode query -C channel-flight-chain -n flightchain -c '{"Args":["getFlightQuery","{\"selector\":{\"flightData.originDate\":\"2019-01-07\"}}"]}'
peer chaincode query -C channel-flight-chain -n flightchain -c '{"Args":["getFlightQuery","{\"selector\":{\"flightData.operatingAirline.iataCode\":\"BA\"}}"]}'
```
Note: best practice is to create index(es) for your chaincode to optimize query performance.
To do this you need to create index with path /META-INF/statedb/couchdb/indexes
see [fabric docs](https://hyperledger-fabric.readthedocs.io/en/release-1.4/couchdb_tutorial.html)

### updating channel config to support private data
Fabric allows specification of capabilities for orderers, peers or both (Channel) in order to set minimum standard accross network.
This allows network to be updated while maintaining backward compatability.
In order to support private data, need to be runnning on 1.2 or above
```
Capabilities:
    # Channel capabilities apply to both the orderers and the peers and must be
    # supported by both.
    # Set the value of the capability to true to require it.
    Channel: &ChannelCapabilities
        # V1.3 for Channel is a catchall flag for behavior which has been
        # determined to be desired for all orderers and peers running at the v1.3.x
        # level, but which would be incompatible with orderers and peers from
        # prior releases.
        # Prior to enabling V1.3 channel capabilities, ensure that all
        # orderers and peers on a channel are at v1.3.0 or later.
        V1_3: true

```

### Deploying to k8s

 https://repo.aticloud.aero/blockchain/sandbox/AdministrationProject/wikis/chaincode-deployment-flightchain
 
Exec into cli node(s) (e.g. cli-sitaeu, cli-sitaus)

clone code from gitlab and build locally
```
git clone https://gitlab.com/FlightChain3/FlightChainSmartContract.git
cd FlightChainSmartContract/
npm i
npm i -g gulp-cli
gulp compile
```

check for previous installation of flightchain
```
peer chaincode list --installed
```

install the code on all peers (note version number should be incremented if there are previous versions installed)
```
export CHAIN_CODE_VERSION=1.0
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fabric12/orderers/orderer1st-sita.fabric12/msp/tlscacerts/tlsca.fabric12-cert.pem 


CORE_PEER_ADDRESS="peer1st-sitaeu.fabric12:7051"
peer chaincode install -n flightchain -v $CHAIN_CODE_VERSION -l "node" -p "/opt/gopath/src/github.com/hyperledger/fabric/peer/FlightChainSmartContract" --tls --cafile=$ORDERER_CA
CORE_PEER_ADDRESS="peer2nd-sitaeu.fabric12:7051"
peer chaincode install -n flightchain -v $CHAIN_CODE_VERSION -l "node" -p "/opt/gopath/src/github.com/hyperledger/fabric/peer/FlightChainSmartContract" --tls --cafile=$ORDERER_CA
```

Note: if deploying to US nodes, repeat cmds using sitaus peer address from cli-sitaus

instantiate OR upgrade the code (upgrade in the case of existing flightchain installed)
```
peer chaincode instantiate -o orderer1st-sita.fabric12:7050 --channelID sitatranchannel -n flightchain -l node -v $CHAIN_CODE_VERSION -c '{"Args":["FlightChainContract:initLedger"]}' -P "OR ('sitaeuMSP.member','sitausMSP.member')" --tls --cafile=$ORDERER_CA --collections-config /opt/gopath/src/github.com/hyperledger/fabric/peer/FlightChainSmartContract/privateData/collectionConfig.json
# OR
peer chaincode upgrade -o orderer1st-sita.fabric12:7050 --channelID sitatranchannel -n flightchain -l node -v $CHAIN_CODE_VERSION -c '{"Args":["FlightChainContract:initLedger"]}' -P "OR ('sitaeuMSP.member','sitausMSP.member')" --tls --cafile=$ORDERER_CA --collections-config /opt/gopath/src/github.com/hyperledger/fabric/peer/FlightChainSmartContract/privateData/collectionConfig.json
```

