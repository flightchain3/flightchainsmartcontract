import {ACRISFlight} from '../src/model/ACRISFlight';
import { FlightChainValidator } from '../src/flightChainValidator';
import {IFlightChainData} from '../src/model/IFlightChainData';
import {FlightDataHelper} from '../src/flightDataHelper';
import {FlightChainContract} from "../src/flightChainSmartContract";
import {ICallerIdentity} from "../src/model/ICallerIdentity";

const rewire = require('rewire');
const flightChainContract = rewire('../src/flightChainSmartContract');

const mockConsole = jasmine.createSpyObj('console', ['info', 'log', 'warn']);
const mockContext = jasmine.createSpyObj('context', ['stub']);

describe('FlightChainContract', () => {
    let origPackageVersion;

    beforeEach(() => {
        flightChainContract.__set__('console', mockConsole);
        origPackageVersion = process.env.npm_package_version;

        // reset console log calls between tests - so we ensure log was called for *this* scenario
        mockConsole.log.calls.reset();
    });

    afterEach(() => {
        // restore package version - after test
        process.env.npm_package_version = origPackageVersion;
    });

    describe('getVersion', () => {
        it ('should log the fact that it has been invoked', async () => {
            const contract = new flightChainContract.FlightChainContract();
            const version = await contract.getVersion(mockContext);

            expect(mockConsole.info).toHaveBeenCalled();
        });

        it ('should return version based on npm package version', async() => {
            process.env.npm_package_version = "1.3.2";

            const contract = new flightChainContract.FlightChainContract();
            const version = await contract.getVersion(mockContext);

            expect(version).toEqual('1.3.2');
        });
    });

    describe('createFlight', () => {
        it ('should throw error if caller identity is not valid', async () =>{
            spyOn(FlightChainValidator, 'verifyIdentity').and.throwError('certificate contains invalid Iata code');

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.createFlight(mockContext, '');
                fail('unexpected success createFlight - expected error for caller identity')
            } catch(ex) {
                expect(ex.message).toEqual('certificate contains invalid Iata code');
            }
        });

        it ('should throw error if flight json is not valid', async () =>{
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'BA', iataCodes: ['BA']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.throwError('something wrong with the json');

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.createFlight(mockContext, '');
                fail('unexpected success createFlight - expected error for missing key fields')
            } catch(ex) {
                expect(ex.message).toEqual('something wrong with the json');
            }
        });

        it ('should throw an error if iata code from certificate is not allowed update flight', async () => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'BA', iataCodes: ['BA']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.returnValue(<ACRISFlight> {operatingAirline: { iataCode: 'EI'}, departureAirport: 'DUB', arrivalAirport: 'JFK'});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.throwError('airline does not have permission to update this flight');

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.createFlight(mockContext, '');
                fail('unexpected success createFlight - expected error does not permission to update flight')
            } catch(ex) {
                expect(ex.message).toEqual('airline does not have permission to update this flight');
            }
        });

        it('should update flight when one already exists based on the flight key', async() => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.returnValue(<ACRISFlight> {
                operatingAirline: { iataCode: 'EI'},
                departureAirport: 'DUB', arrivalAirport: 'JFK',
                flightNumber: {trackNumber: '1234'},
                originDate: '2019-03-17'});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.returnValue(true);

            const mockCtx = { stub: {getState: jasmine.createSpy(), putPrivateData: jasmine.createSpy(), getTransient: jasmine.createSpy()}};
            mockCtx.stub.getState.and.returnValue(true);

            const contract = new flightChainContract.FlightChainContract();

            // mock out the actual update function - we test that separately
            contract.updateFlight = (ctx, flightKey, flightJson) => { return true;}

            await contract.createFlight(mockCtx, '');

            expect(mockConsole.log).toHaveBeenCalledWith('A flight with this key: 2019-03-17DUBEI1234 already exists. this data will be merged.')
        });

        it('should create flight when none already exists based on the flight key', async() => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.returnValue(<ACRISFlight> {
                operatingAirline: { iataCode: 'EI'},
                departureAirport: 'DUB', arrivalAirport: 'JFK',
                flightNumber: {trackNumber: '1234'},
                originDate: '2019-03-17'});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.returnValue(true);

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient'])};
            mockCtx.stub.getState.and.returnValue(false);
            mockCtx.stub.getTxID.and.returnValue('tx123');

            const contract = new flightChainContract.FlightChainContract();
            await contract.createFlight(mockCtx, '');

            expect(mockConsole.log).toHaveBeenCalledWith('No existing data for this flight, a new record will be created.')
        });
    });

    describe('updateFlight', () => {
        it ('should throw error if caller identity is not valid', async () =>{
            spyOn(FlightChainValidator, 'verifyIdentity').and.throwError('certificate contains invalid Iata code');

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.updateFlight(mockContext, '','');
                fail('unexpected success updateFlight - expected error for caller identity')
            } catch(ex) {
                expect(ex.message).toEqual('certificate contains invalid Iata code');
            }
        });

        it ('should throw an error if iata code from certificate is not allowed update flight', async () => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'BA', iataCodes: ['BA']});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.throwError('airline does not have permission to update this flight');
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState'])};
            mockCtx.stub.getState.and.returnValue(FlightDataHelper.serializeFlight(<IFlightChainData> { flightKey: 'somekey', flightData: <ACRISFlight> {
                    operatingAirline: { iataCode: 'EI'},
                    departureAirport: 'DUB', arrivalAirport: 'JFK',
                    flightNumber: {trackNumber: '1234'},
                    originDate: '2019-03-17'}
            }));

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.updateFlight(mockCtx, '', '');
                fail('unexpected success createFlight - expected error does not permission to update flight')
            } catch(ex) {
                expect(ex.message).toEqual('airline does not have permission to update this flight');
            }
        });

        it('should throw error when no data exists based on the flight key', async() => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.returnValue(<ACRISFlight> {
                operatingAirline: { iataCode: 'EI'},
                departureAirport: 'DUB', arrivalAirport: 'JFK',
                flightNumber: {trackNumber: '1234'},
                originDate: '2019-03-17'});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.returnValue(true);

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState'])};
            mockCtx.stub.getState.and.returnValue(false);

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.updateFlight(mockCtx, 'somekey', '');
                fail('unexpected success createFlight - expected error does not permission to update flight')
            } catch(ex) {
                expect(ex.message).toEqual('No flight with the key "somekey" exists. It must be created first.');
            }
        });

        it ('should throw error when attempt made to update the flight key', async() => {
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'verifyFlightKeyFields').and.returnValue(<ACRISFlight> {
                operatingAirline: { iataCode: 'EI'},
                departureAirport: 'DUB', arrivalAirport: 'JFK',
                flightNumber: {trackNumber: '1234'},
                originDate: '2019-03-17'});
            spyOn(FlightChainValidator, 'canIdentityCreateOrUpdateFlight').and.returnValue(true);
            spyOn(FlightChainValidator, 'verifyKeyNotModified').and.throwError('invalid attempt to update flight key');

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState'])};
            mockCtx.stub.getState.and.returnValue(FlightDataHelper.serializeFlight(<IFlightChainData> { flightKey: 'somekey', flightData: <ACRISFlight> {
                    operatingAirline: { iataCode: 'EI'},
                    departureAirport: 'DUB', arrivalAirport: 'JFK',
                    flightNumber: {trackNumber: '1234'},
                    originDate: '2019-03-17'}
            }));

            const contract = new flightChainContract.FlightChainContract();
            try {
                await contract.updateFlight(mockCtx, 'somekey', '{"departureAirport": "LHR"}');
                fail('unexpected success createFlight - expected error does not permission to update flight')
            } catch(ex) {
                expect(ex.message).toEqual('invalid attempt to update flight key');
            }
        });
    });

    describe('getFlight', () => {
        it('should warn when there is no data matching key', async() => {
            const contract = new flightChainContract.FlightChainContract();
            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState'])};
            mockCtx.stub.getState.and.returnValue(null);

            await contract.getFlight(mockCtx, 'someKey123');

            expect(mockConsole.warn).toHaveBeenCalledWith('getFlight - no data for someKey123')
        });

        it('should return flight when there is data matching key', async() => {
            const flightData = {
                docType: 'flight',
                flightData: {
                    operatingAirline: {iataCode: 'EI'},
                    departureAirport: 'DUB', arrivalAirport: 'JFK',
                    flightNumber: {trackNumber: '1234'},
                    originDate: '2019-03-17'
                },
                flightKey: "fk1234",
                txId: 'tx1234',
                updaterId: 'BA'
            };
            const contract = new flightChainContract.FlightChainContract();

            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'canReadCountData').and.returnValue(false);
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'getPrivateData'])};
            mockCtx.stub.getState.and.returnValue(FlightDataHelper.serializeFlight(flightData));

            const result = await contract.getFlight(mockCtx, 'someKey123');

            expect(result).toEqual(flightData);
        });

        it('should return flight plus private data when there is data matching key and allowed to read private data', async() => {
            const flightData = {
                docType: 'flight',
                flightData: {
                    operatingAirline: {iataCode: 'EI'},
                    departureAirport: 'DUB', arrivalAirport: 'JFK',
                    flightNumber: {trackNumber: '1234'},
                    originDate: '2019-03-17'
                },
                flightKey: "fk1234",
                txId: 'tx1234',
                updaterId: 'BA'
            };
            const paxCounts = [{"elementType": "pax", "count": "12"}];
            const bagCounts = [{"elementType": "bag", "count": "13"}];

            const contract = new flightChainContract.FlightChainContract();

            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'canReadCountData').and.returnValue(true);
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'getPrivateData'])};
            mockCtx.stub.getState.and.returnValue(FlightDataHelper.serializeFlight(flightData));
            mockCtx.stub.getPrivateData.and.callFake((collection, key) => {
               if (collection === 'paxCountCollection') {
                    return FlightDataHelper.serializeCount(paxCounts);
               } else {
                   return FlightDataHelper.serializeCount(bagCounts);
               }
            });

            const result = await contract.getFlight(mockCtx, 'someKey123');

            expect(result.flightData.extensions).toEqual({counts:
                 [ {elementType: "pax", count: "12"}, { elementType: "bag", count: "13" } ]
            });
        });

        it('should return flight without private data when there is data matching key and NOT allowed to read private data', async() => {
            const flightData = {
                docType: 'flight',
                flightData: {
                    operatingAirline: {iataCode: 'EI'},
                    departureAirport: 'DUB', arrivalAirport: 'JFK',
                    flightNumber: {trackNumber: '1234'},
                    originDate: '2019-03-17'
                },
                flightKey: "fk1234",
                txId: 'tx1234',
                updaterId: 'BA'
            };
            const paxCounts = [{"elementType": "pax", "count": "12"}];
            const bagCounts = [{"elementType": "bag", "count": "13"}];

            const contract = new flightChainContract.FlightChainContract();

            spyOn(FlightChainValidator, 'verifyIdentity').and.returnValue( <ICallerIdentity> { owner: 'EI', iataCodes: ['EI']});
            spyOn(FlightChainValidator, 'canReadCountData').and.returnValue(false);
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'getPrivateData'])};
            mockCtx.stub.getState.and.returnValue(FlightDataHelper.serializeFlight(flightData));
            mockCtx.stub.getPrivateData.and.callFake((collection, key) => {
                if (collection === 'paxCountCollection') {
                    return FlightDataHelper.serializeCount(paxCounts);
                } else {
                    return FlightDataHelper.serializeCount(bagCounts);
                }
            });

            const result = await contract.getFlight(mockCtx, 'someKey123');

            expect(result.flightData.extensions).toBeUndefined();
        });
    });

});
