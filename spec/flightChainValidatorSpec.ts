import {ACRISFlight} from '../src/model/ACRISFlight';
import {ICallerIdentity} from "../src/model/ICallerIdentity";

const flightChainValidator = require('../src/flightChainValidator');

describe('flightChainValidator', () => {
    const mockClientIdentity = jasmine.createSpyObj('clientIdentity', ['getAttributeValue']);

    beforeEach(() => {

    });

    afterEach(() => {

    });

    describe('verifyIdentity', () => {

        it ('should return iataCode property if valid single iata code in client identity', async () => {
            mockClientIdentity.getAttributeValue.and.returnValue('EI');

            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);

            expect(result.iataCodes[0]).toEqual('EI');
        });

        it ('should return array of iataCodes property if multiple iata code in client identity', async () => {
            mockClientIdentity.getAttributeValue.and.returnValue('EI|BA');

            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);

            expect(result.iataCodes[0]).toEqual('EI');
            expect(result.iataCodes[1]).toEqual('BA');
        });

        it ('should return owner property if set in client identity', async () => {
            mockClientIdentity.getAttributeValue.and.callFake((attr) => {
                if (attr === 'iata-code') {
                    return 'EI|BA';
                } else {
                    return 'IAG';
                }
            });

            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);

            expect(result.owner).toEqual('IAG');
        });

        it ('should default owner property to iatacode if not set in client identity', async () => {
            mockClientIdentity.getAttributeValue.and.returnValue('EI');

            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);

            expect(result.owner).toEqual('EI');
        });

        it ('should throw error if no iata code in client identity', async () => {
            mockClientIdentity.getAttributeValue.and.returnValue(undefined);

            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);
                fail('unexpected success verifying Identity');
            } catch (ex) {
                expect(ex.message).toEqual('verify Identity failed: missing iata-code "undefined"');
            }

        });

        it ('should throw error if iata code invalid in client identity', async() => {
            mockClientIdentity.getAttributeValue.and.returnValue('someNonsense');

            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyIdentity(mockClientIdentity);
                fail('unexpected success verifying Identity');
            } catch (ex) {
                expect(ex.message).toEqual('verify Identity failed: Certificate contains invalid iata-code "someNonsense"');
            }
        });
    });

    describe('verifyFlightKeyFields', () => {
        it('should throw error if flight json cannot be parsed', async () => {
            //const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('this is not valid json at all');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data: cannot parse json');
            }
        });

        it('should throw error if flight json does not contain operating airline', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"valid":"json","but":"not","containing":"flight"}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid flight.operatingAirline.iataCode set.');
            }
        });

        it('should throw error if flight json does not contain departure airport', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"operatingAirline":{"iataCode": "EI"},"but":"no","departure":"airport"}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid 3 char flight.departureAirport set.');
            }
        });

        it('should throw error if flight json does not contain arrival airport', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"operatingAirline":{"iataCode": "EI"},"departureAirport":"DUB","but":"not","arrival":"airport"}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid 3 char flight.arrivalAirport set.');
            }
        });

        it('should throw error if flight json does not contain flightNumber', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"operatingAirline":{"iataCode": "EI"},"departureAirport":"DUB","arrivalAirport":"JFK"}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid flight.flightNumber.trackNumber (1-4 digits) set.');
            }
        });

        it('should throw error if flight json contains invalid flightNumber', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"operatingAirline":{"iataCode": "EI"},"departureAirport":"DUB","arrivalAirport":"JFK", "flightNumber": { "trackNumber": "12345"}}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid flight.flightNumber.trackNumber (1-4 digits) set.');
            }
        });

        it('should throw error if flight json does not contain originDate', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.verifyFlightKeyFields('{"operatingAirline":{"iataCode": "EI"},"departureAirport":"DUB","arrivalAirport":"JFK","flightNumber":{"trackNumber":"0123"}}');
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, there is no valid flight.originDate set (e.g. 2018-09-13).');
            }
        });
    });

    describe('canIdentityCreateOrUpdateFlight', () => {
        it('should throw error when supplied airline iata code does not match flight operating airline', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.canIdentityCreateOrUpdateFlight(<ICallerIdentity> { iataCodes: ['BA']}, <ACRISFlight> { operatingAirline: {iataCode: 'EI'}});
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('The supplied iata code: ["BA"] does not match the airline EI or the departure airport undefined or the arrival airport undefined');
            }
        });

        it('should throw error when supplied airport iata code does not match dep or arr airport', async () => {
            const validator = new flightChainValidator.FlightChainValidator();
            try {
                await flightChainValidator.FlightChainValidator.canIdentityCreateOrUpdateFlight(<ICallerIdentity> { iataCodes: ['LHR']}, <ACRISFlight> { operatingAirline: {iataCode: 'EI'}, departureAirport: 'DUB', arrivalAirport: 'JFK'});
                fail('unexpected success verifying flight json');
            } catch (ex) {
                expect(ex.message).toEqual('The supplied iata code: ["LHR"] does not match the airline EI or the departure airport DUB or the arrival airport JFK');
            }
        });

        it ('should return true when supplied airline iata code matches operating airline', async() => {
            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.canIdentityCreateOrUpdateFlight(<ICallerIdentity> { iataCodes: ['BA']}, <ACRISFlight> { operatingAirline: {iataCode: 'BA'}});
            expect(result).toBe(true);
        });

        it ('should return true when one of multiple supplied airline iata code matches operating airline', async() => {
            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.canIdentityCreateOrUpdateFlight(<ICallerIdentity> { iataCodes: ['AA', 'BA', 'EI']}, <ACRISFlight> { operatingAirline: {iataCode: 'BA'}});
            expect(result).toBe(true);
        });

        it ('should return true when one of multiple supplied airport iata code matches departing airport', async() => {
            const validator = new flightChainValidator.FlightChainValidator();
            const result = await flightChainValidator.FlightChainValidator.canIdentityCreateOrUpdateFlight(<ICallerIdentity> { iataCodes: ['DUB', 'LHR', 'JFK']}, <ACRISFlight> { operatingAirline: {iataCode: 'EI'}, departureAirport: 'DUB', arrivalAirport: 'CDG'});
            expect(result).toBe(true);
        });
    });
});
