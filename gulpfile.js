const gulp = require('gulp');
const ts = require('gulp-typescript');
const jasmine = require('gulp-jasmine');
const coverage = require('gulp-coverage');
const shell = require('gulp-shell');
const lint = require('gulp-tslint');
const del = require('del');
const execSync = require('child_process').execSync;

const tsProject = ts.createProject('tsconfig.json');

// cleanup previous dist folder contents
gulp.task('clean', function (done) {
    del(['dist/**/*.*']);
    done();
});

// lint
gulp.task("lint", () =>
    gulp.src("src/**/*.ts")
        .pipe(lint({
            formatter: "verbose"
        }))
        .pipe(lint.report({
            summarizeFailureOutput: true
        }))
);

// compile typescript to js in dist folder
gulp.task('compile', function() {
    var tsResult = gulp.src("src/**/*.ts")
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('dist'));
});

// run tests
gulp.task('test', function(done) {
    try {
        execSync('ts-node node_modules/jasmine/bin/jasmine').toString();
   } catch(ex) {
        console.error('A test failed: ' + ex.stdout);
    }

    done();
});

gulp.task('default', gulp.series('clean', 'lint', 'test', 'compile'));