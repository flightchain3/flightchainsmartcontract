import {Iterators} from 'fabric-shim';
import {ACRISFlight, PaxCount} from './model/ACRISFlight';
import {IFlightChainData} from './model/IFlightChainData';
import {IFlightChainDataHistory} from './model/IFlightChainDataHistory';

export class FlightDataHelper {
    public static getUniqueFlightKey(flight: ACRISFlight) {
        let flightNum = flight.flightNumber.trackNumber;
        while (flightNum.length < 4) {
            flightNum = '0' + flightNum;
        }
        const flightKey = flight.originDate + flight.departureAirport + flight.operatingAirline.iataCode + flightNum;
        console.debug('getUniqueFlightKey: ', flightKey);
        return flightKey;
    }

    public static serializeFlight(flight: IFlightChainData): Buffer {
        return Buffer.from(JSON.stringify(flight));
    }

    public static serializeCount(count: PaxCount): Buffer {
        return Buffer.from(JSON.stringify(count));
    }

    public static bufferToObject(buffer: Buffer): IFlightChainData {
        if (buffer === null) {
            return null;
        }

        const bufferString = buffer.toString('utf8');
        if (bufferString.length <= 0) {
            return null;
        }

        try {
            return JSON.parse(bufferString);
        } catch (err) {
            console.error('Error parsing buffer to JSON', bufferString);
            return null;
        }
    }

    public static bufferToArray(buffer: Buffer): PaxCount[] {
        if (buffer === null) {
            return null;
        }

        const bufferString = buffer.toString('utf8');
        if (bufferString.length <= 0) {
            return null;
        }

        try {
            return JSON.parse(bufferString);
        } catch (err) {
            console.error('Error parsing buffer to JSON', bufferString);
            return null;
        }
    }

    public static async iteratorToHistory(iterator: Iterators.HistoryQueryIterator):
        Promise<IFlightChainDataHistory[]> {
        const results = [];

        let res;
        while (true) {
            res = await iterator.next();
            if (res.value && res.value.value) {
                const flight = this.bufferToObject(res.value.value);
                // TODO need to set timestamp here (needed for history query)
                const parsedItem: IFlightChainDataHistory = {
                    flightData: flight,
                    key: res.value.key,
                    timestamp: res.value.timestamp,
                };
                results.push(parsedItem);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                break;
            }
        }

        return results;
    }

    public static getPublicFlightData(flight: ACRISFlight): ACRISFlight {
        const publicFlightData = flight;
        publicFlightData.extensions = null;

        return publicFlightData;
    }

    public static getPaxCounts(transientMap: any): PaxCount[] {
        const paxCounts = [];
        const counts = FlightDataHelper.getCountsFromTransientMap(transientMap);

        if (counts && counts.length > 0) {
            counts.forEach((count) => {
                if (count.elementType.toLowerCase() === 'pax') {
                    if (!count.category) {
                        count.category = 'total';
                    }
                    paxCounts.push(count);
                }
            });
        }

        return paxCounts;
    }

    public static getBagCounts(transientMap: any): PaxCount[] {
        const bagCounts = [];
        const counts = FlightDataHelper.getCountsFromTransientMap(transientMap);

        if (counts && counts.length > 0) {
            counts.forEach((count) => {
                if (count.elementType.toLowerCase() === 'bag') {
                    if (!count.category) {
                        count.category = 'total';
                    }
                    bagCounts.push(count);
                }
            });
        }

        return bagCounts;
    }

    public static getMergedPaxCounts(existingPaxCounts: PaxCount[], updatedFlightTransientMap: object): PaxCount[] {
        let mergedPaxCounts = [];
        if (!existingPaxCounts) {
            existingPaxCounts = [];
        }
        const updatedPaxCounts = this.getPaxCounts(updatedFlightTransientMap);
        if (updatedPaxCounts.length > 0) {
           mergedPaxCounts = existingPaxCounts;
           updatedPaxCounts.forEach((updatedCount) => {
               let isExistingCount = false;
               for (const mergedBagCount of mergedPaxCounts) {
                   if (updatedCount.category === mergedBagCount.category
                       && updatedCount.assistanceQualifier === mergedBagCount.assistanceQualifier
                       && updatedCount.transferQualifier === mergedBagCount.transferQualifier) {
                       mergedBagCount.count = updatedCount.count;
                       isExistingCount = true;
                   }
               }
               if (!isExistingCount) {
                   mergedPaxCounts.push(updatedCount);
               }
           });
        }

        return mergedPaxCounts;
    }

    public static getMergedBagCounts(existingBagCounts: PaxCount[], updatedFlightTransientMap: object): PaxCount[] {
        let mergedBagCounts = [];
        if (!existingBagCounts) {
            existingBagCounts  = [];
        }
        const updatedBagCounts = this.getBagCounts(updatedFlightTransientMap);
        if (updatedBagCounts.length > 0) {
            mergedBagCounts = existingBagCounts;
            updatedBagCounts.forEach((updatedCount) => {
                let isExistingCount = false;
                for (const mergedBagCount of mergedBagCounts) {
                    if (updatedCount.category === mergedBagCount.category
                        && updatedCount.assistanceQualifier === mergedBagCount.assistanceQualifier
                        && updatedCount.transferQualifier === mergedBagCount.transferQualifier) {
                        mergedBagCount.count = updatedCount.count;
                        isExistingCount = true;
                    }
                }
                if (!isExistingCount) {
                    mergedBagCounts.push(updatedCount);
                }
            });
        }

        return mergedBagCounts;
    }

    private static getCountsFromTransientMap(transientMap: Map<string, Buffer>): PaxCount[] {
        if (transientMap) {
            const iterator1 = transientMap.entries();
            const entry = iterator1.next();
            while (!entry.done) {
                const key = entry.value[0];
                const value = entry.value[1];
                if (key === 'counts') {
                    const counts = JSON.parse(value.toString('utf8'));
                    return counts;
                }
            }
        }
        return null;
    }
}
