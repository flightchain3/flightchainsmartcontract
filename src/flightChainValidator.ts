import { ClientIdentity } from 'fabric-shim';

import { FlightDataHelper } from './flightDataHelper';
import { ACRISFlight } from './model/ACRISFlight';
import {ICallerIdentity} from './model/ICallerIdentity';

export class FlightChainValidator {
    public static verifyIdentity(clientIdentity: ClientIdentity): ICallerIdentity {
        const iataCode = clientIdentity.getAttributeValue(FlightChainValidator.ATTR_IATA_CODES);
        let owner = clientIdentity.getAttributeValue(FlightChainValidator.ATTR_OWNER);

        // TODO errors not being passed back - falling back to default error message - logged ok
        if (!iataCode) {
            const msg = 'verify Identity failed: missing iata-code "' + iataCode + '"';
            console.warn(msg);
            throw new Error(msg);
        }
        if (!owner) {
            owner = iataCode;
        }

        const iataCodes = this.trimChar(iataCode, '|').split('|');
        iataCodes.forEach((code) => {
            if (code.length > 3) {
                const msg = 'verify Identity failed: Certificate contains invalid iata-code "' + code + '"';
                console.warn(msg);
                throw new Error(msg);
            }
        });
        return { owner, iataCodes} as ICallerIdentity;
    }

    public static verifyFlightKeyFields(flightJson: string | ACRISFlight): ACRISFlight {
        let flight: ACRISFlight;
        if (typeof flightJson === 'string') {
            try {
                flight = JSON.parse(flightJson);
            } catch {
                console.warn('cannot parse flight json: ' + flightJson);
                throw new Error('Invalid flight data: cannot parse json');
            }
        } else {
            flight = flightJson;
        }

        if (!flight.operatingAirline
            || !flight.operatingAirline.iataCode
            || flight.operatingAirline.iataCode.length !== 2) {
            const msg = 'Invalid flight data, there is no valid flight.operatingAirline.iataCode set.';
            console.warn(msg, flight);
            throw new Error(msg);
        }

        if (!flight.departureAirport || flight.departureAirport.length !== 3) {
            const msg = 'Invalid flight data, there is no valid 3 char flight.departureAirport set.';
            console.warn(msg, flight);
            throw new Error(msg);
        }

        if (!flight.arrivalAirport || flight.arrivalAirport.length !== 3) {
            const msg = 'Invalid flight data, there is no valid 3 char flight.arrivalAirport set.';
            console.warn(msg, flight);
            throw new Error(msg);
        }
        if (!flight || !flight.flightNumber
            || !flight.flightNumber.trackNumber
            || flight.flightNumber.trackNumber.length < 1
            || flight.flightNumber.trackNumber.length > 4) {
            const msg = 'Invalid flight data, there is no valid flight.flightNumber.trackNumber (1-4 digits) set.';
            console.log(msg, flight);
            throw new Error(msg);
        }
        if (!flight || !flight.originDate || !Date.parse(flight.originDate)) {
            const msg = 'Invalid flight data, there is no valid flight.originDate set (e.g. 2018-09-13).';
            console.log(msg, flight);
            throw new Error(msg);
        }

        return flight;
    }

    public static verifyKeyNotModified(originalFlightKey, updatedFlight: ACRISFlight) {
        const updatedDataKey = FlightDataHelper.getUniqueFlightKey(updatedFlight);
        if (updatedDataKey !== originalFlightKey) {
            const msg = 'You are not permitted to update data that will modify the flight key '
                + '(origin date, departure airport, airline iata code or flight number)';
            console.warn(msg, updatedFlight);
            throw new Error(msg);
        }
    }

    public static canIdentityCreateOrUpdateFlight(callerIdentity: ICallerIdentity, flight: ACRISFlight): boolean {
        const match = this.isIdentityTheOwningAirlineOrAirport(callerIdentity, flight);

        if (!match) {
            const msg = 'The supplied iata code: ' + JSON.stringify(callerIdentity.iataCodes) +
                ' does not match the airline ' + this.getOperatingAirline(flight) +
                ' or the departure airport ' + this.getDepartureAirport(flight) +
                 ' or the arrival airport ' + this.getArrivalAirport(flight);
            console.log(msg);
            throw new Error(msg);
        }

        return match;
    }

    public static canReadCountData(callerIdentity: ICallerIdentity, flight: ACRISFlight): boolean {
        // is caller one of the airports or airline who "owns" flight?
        // TODO - may want to revisit depending on customer workshops
        return this.isIdentityTheOwningAirlineOrAirport(callerIdentity, flight);
    }

    private static ATTR_IATA_CODES = 'iata-code';
    private static ATTR_OWNER = 'owner';

    private static isIdentityTheOwningAirlineOrAirport(callerIdentity: ICallerIdentity, flight: ACRISFlight): boolean {
        let match = false;

        for (const iataCode of callerIdentity.iataCodes) {
            if (this.isAirline(iataCode)) {
                const airline = this.getOperatingAirline(flight);
                if (airline.toUpperCase() === iataCode.toUpperCase()) {
                    match = true;
                    break;
                }
            } else {
                const departureAirport = this.getDepartureAirport(flight);
                const arrivalAirport = this.getArrivalAirport(flight);
                if (iataCode.toUpperCase() === departureAirport.toUpperCase()
                    || iataCode.toUpperCase() === arrivalAirport.toUpperCase()) {
                    match = true;
                    break;
                }
            }
        }

        return match;
    }

    private static isAirline(iataCode: string) {
        return iataCode.length === 2;
    }

    private static getOperatingAirline(flight: ACRISFlight) {
        return flight.operatingAirline.iataCode;
    }

    private static getDepartureAirport(flight: ACRISFlight) {
        return flight.departureAirport;
    }

    private static getArrivalAirport(flight: ACRISFlight) {
        return flight.arrivalAirport;
    }

    private static trimChar(stringToTrim, charToRemove) {
        while (stringToTrim.charAt(0) === charToRemove) {
            stringToTrim = stringToTrim.substring(1);
        }

        while (stringToTrim.charAt(stringToTrim.length - 1) === charToRemove) {
            stringToTrim = stringToTrim.substring(0, stringToTrim.length - 1);
        }

        return stringToTrim;
    }
}
