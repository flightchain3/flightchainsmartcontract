import deepmerge = require('deepmerge');
import { Context, Contract } from 'fabric-contract-api';
import { FlightChainValidator } from './flightChainValidator';

import { FlightDataHelper } from './flightDataHelper';
import { ACRISFlight } from './model/ACRISFlight';
import { IFlightChainData } from './model/IFlightChainData';

export class FlightChainContract extends Contract {

    private PAXCOUNT_COLLECTION_NAME = 'paxCountCollection';
    private BAGCOUNT_COLLECTION_NAME = 'bagCountCollection';

    public async initLedger(ctx: Context) {
        console.info('=================== START: Initialize Ledger =====================');
    }

    public async getVersion(ctx: Context): Promise<string> {
        console.info('===================    START: getVersion    =====================');
        return new Promise<string>((resolve, reject) => {
            resolve(process.env.npm_package_version);
        });
    }

    public async getFlight(ctx: Context, flightKey: string) {
        console.info('===================   START:  getFlight    =====================');

        const rawData = await ctx.stub.getState(flightKey);
        const flightData: IFlightChainData = FlightDataHelper.bufferToObject(rawData);
        const callerIdentity = FlightChainValidator.verifyIdentity(ctx.clientIdentity);
        if (!flightData) {
            console.warn('getFlight - no data for ' + flightKey);
        } else {
            if (FlightChainValidator.canReadCountData(callerIdentity, flightData.flightData)) {
                console.info('getting private data. identity: ' + JSON.stringify(callerIdentity));
                const paxCounts = await ctx.stub.getPrivateData(this.PAXCOUNT_COLLECTION_NAME, flightKey);
                const bagCounts = await ctx.stub.getPrivateData(this.BAGCOUNT_COLLECTION_NAME, flightKey);
                if (paxCounts !== null) {
                    flightData.flightData.extensions = { counts:  FlightDataHelper.bufferToArray(paxCounts) };
                } else {
                    if (bagCounts !== null) {
                        if (flightData.flightData.extensions.counts !== null) {
                            flightData.flightData.extensions.counts = flightData.flightData.extensions.counts
                                .concat(FlightDataHelper.bufferToArray(bagCounts));
                        } else {
                            flightData.flightData.extensions.counts = {
                                counts:  FlightDataHelper.bufferToArray(bagCounts) };
                        }
                    }
                }
            }
        }

        return flightData;
    }

    public async getFlightHistory(ctx, flightkey: string) {
        console.info('===================   START:  getFlight    =====================');
        const rawData = await ctx.stub.getHistoryForKey(flightkey);
        return await FlightDataHelper.iteratorToHistory(rawData);
    }

    public async getFlightQuery(ctx, query: string) {
        console.info('===================   START:  getFlightQuery    =====================');
        const rawData = await ctx.stub.getQueryResult(query);
        return await FlightDataHelper.iteratorToHistory(rawData);
    }

    public async createFlight(ctx: Context, flightJson: string) {
        console.info('===================   START: createFlight   =====================');
        const callerIdentity = FlightChainValidator.verifyIdentity(ctx.clientIdentity);
        const flight = FlightChainValidator.verifyFlightKeyFields(flightJson);

        if (FlightChainValidator.canIdentityCreateOrUpdateFlight(callerIdentity, flight)) {
            const flightKey = FlightDataHelper.getUniqueFlightKey(flight);
            const isExistingFlight = FlightDataHelper.bufferToObject(await ctx.stub.getState(flightKey));
            if (isExistingFlight) {
                console.log('A flight with this key: ' + flightKey + ' already exists. this data will be merged.');
                this.updateFlight(ctx, flightKey, flightJson);
            } else {
                console.log('No existing data for this flight, a new record will be created.');
                const flightObject: IFlightChainData = {
                    docType: 'flight',
                    flightData: flight,
                    flightKey,
                    txId:  ctx.stub.getTxID(),
                    updaterId: callerIdentity.owner,
                };
                await ctx.stub.putState(flightKey, FlightDataHelper.serializeFlight(flightObject));
            }
            const privateData = ctx.stub.getTransient();
            if (privateData) {
                console.log('checking for private data');
                const paxCounts = FlightDataHelper.getPaxCounts(privateData);
                if (paxCounts && paxCounts.length > 0) {
                    console.log('saving pax counts to private data collections');
                    await ctx.stub.putPrivateData(this.PAXCOUNT_COLLECTION_NAME, flightKey,
                        FlightDataHelper.serializeCount(paxCounts));
                }

                const bagCounts = FlightDataHelper.getBagCounts(privateData);
                if (bagCounts && bagCounts.length > 0) {
                    console.log('saving bag counts to private data collections');
                    await ctx.stub.putPrivateData(this.BAGCOUNT_COLLECTION_NAME, flightKey,
                        FlightDataHelper.serializeCount(bagCounts));
                }
            }
        }
        console.info('===================    END: createFlight    =====================');
    }

    public async updateFlight(ctx: Context, flightKey: string, flightJson: string) {
        console.info('===================   START: updateFlight   =====================');
        const callerIdentity = FlightChainValidator.verifyIdentity(ctx.clientIdentity);

        const flightChainData = FlightDataHelper.bufferToObject(await ctx.stub.getState(flightKey));
        if (!flightChainData) {
            const msg = 'No flight with the key "' + flightKey + '" exists. It must be created first.';
            console.warn(msg);
            throw new Error(msg);
        }

        if (FlightChainValidator.canIdentityCreateOrUpdateFlight(callerIdentity, flightChainData.flightData)) {
            const flightDelta = JSON.parse(flightJson);
            const mergedFlight: ACRISFlight = deepmerge(flightChainData.flightData, flightDelta);
            FlightChainValidator.verifyFlightKeyFields(mergedFlight);
            FlightChainValidator.verifyKeyNotModified(flightKey, mergedFlight);

            flightChainData.updaterId = callerIdentity.owner;
            flightChainData.txId = ctx.stub.getTxID();
            flightChainData.flightData = mergedFlight;
            await ctx.stub.putState(flightKey, FlightDataHelper.serializeFlight(flightChainData));
            const privateData = ctx.stub.getTransient();
            if (privateData) {
                const paxCounts = FlightDataHelper.getPaxCounts(privateData);
                const bagCounts = FlightDataHelper.getBagCounts(privateData);

                // TODO code below simply overwrite counts - may want something more subtle based on update vs existing
                if (paxCounts.length > 0) {
                    await ctx.stub.putPrivateData(this.PAXCOUNT_COLLECTION_NAME, flightKey,
                        FlightDataHelper.serializeCount(paxCounts));
                }
                if (bagCounts.length > 0) {
                    await ctx.stub.putPrivateData(this.BAGCOUNT_COLLECTION_NAME, flightKey,
                        FlightDataHelper.serializeCount(bagCounts));
                }
            }

        }
        console.info('===================    END: updateFlight    =====================');
    }

}
