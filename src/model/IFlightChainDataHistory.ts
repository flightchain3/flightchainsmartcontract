import {IFlightChainData} from './IFlightChainData';

export interface IFlightChainDataHistory {
    flightData: IFlightChainData;
    timestamp: object;
    key: string;
}
